livetv.py
=========

Ein Eindatei-Programm in Python, das den Empfang öffentlich-rechtlicher
Sender übers Internet ermöglicht.  Keine Lust auf überladene
Browser-Schnittstellen?  livetv.py hilft.

Die Installation ist trivial: Python und mpv installieren und dann
entweder ``python livetv.py`` sagen oder (besser) die Datei irgendwo in
den Pfad kopieren und ``chmod +x livety.py`` machen.

Etwas mehr zum Thema, inklusive einer kleinen Bedienungsanleitung: 
http://blog.tfiu.de/fernseh-livestreams-mit-python-und-mpv.html

Ich verteile das nach CC-0; den Code habe ich selbst geschrieben,
bei der Stationsliste bin ich mir allerdings nicht ganz sicher (vgl. den
Modul-Docstring; könnte sein, dass das GPL 3 ist, aber dann weiß ich
nicht, wer das Copyright hat).
